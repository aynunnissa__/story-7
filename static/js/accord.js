$(document).ready(function(){
	$('.accordion:first-child').find('.move-up').css('display','none');
	$('.accordion:last-child').find('.move-down').css('display','none');
	$(".an-accordion").click(function(){
		if ($(this).find('.content-acc').css('display') === 'none') {
			$('.content-acc').css('display','none');
			$(this).parent().siblings().children().find('.title-acc').animate({'font-size': '20px'},1500);
			$(this).find('.title-acc').animate({'font-size': '25px'},1500);
			$(this).find('.content-acc').css('display','block');
			$(this).find('.content-acc').animate({opacity:'1'}, 1500);
			$(this).animate({height:'47vh'}, 1000);
			$(this).parent().siblings().children().find('.content-acc').animate({opacity:'0'}, 1000);
			$(this).parent().siblings().find('.an-accordion').animate({height:'7vh'}, 1000);
		} else {
			$('.content-acc').css('display','none');
			$('.content-acc').animate({opacity:'0'}, 1500);
			$('.an-accordion').animate({height :'17vh'}, 1000);
			$('.title-acc').animate({'font-size': '25px'},1500);
		}
	});
	$(".move-down").click(function(){
		x = $(this).parent();
		y = $(this).parent().next();
		x.insertAfter(y);
		x.css({'z-index' : "-=1"});
		y.css({'z-index' : "+=1"});
		$('.move-up').css('display','block')
		$('.move-down').css('display','block')
		$('.accordion:last-child').find('.move-down').css('display','none');
		$('.accordion:first-child').find('.move-up').css('display','none');
	});
	$(".move-up").click(function(){
		x = $(this).parent();
		y = $(this).parent().prev();
		y.insertAfter(x);
		x.css({'z-index' : "+=1"});
		y.css({'z-index' : "-=1"});
		$('.move-up').css('display','block')
		$('.move-down').css('display','block')
		$('.accordion:first-child').find('.move-up').css('display','none');
		$('.accordion:last-child').find('.move-down').css('display','none');
	});	
});