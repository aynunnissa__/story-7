from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver

# Create your tests here.
class AccordionTestCase(TestCase):
    
    def test_url_page_accordion(self):
        response = Client().get('')
        self.assertEquals(200, response.status_code)

    def test_template_accordion(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'main/accordion.html')

    def test_display_template_accordion(self):
        response = Client().get('')
        result = response.content.decode('utf8')

        self.assertIn("Aktivitas Saat Ini", result)
        self.assertIn("Pengalaman Kepanitiaan", result)
        self.assertIn("Prestasi", result)
        self.assertIn("Hobi atau Kesukaan", result)

